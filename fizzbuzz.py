class FizzBuzz:

    #function to test divisibility of number
    def is_divisible_by(self, number, divisor):
        return number % divisor == 0

    # function to return acc to the input
    def says(self,number):
        if self.is_divisible_by( number, 15):
            return "fizzbuzz"
        elif self.is_divisible_by(number, 3):
            return "fizz"
        elif self.is_divisible_by(number, 5):
            return "buzz"
        else:
            return number

    # function to read the file
    def readFromFile(self,filename):
        file=open(filename,"r")
        line=file.readline()
        return line


#object creation
fizzbuzz=FizzBuzz()
for i in range(1,101):
    print(fizzbuzz.says(i))