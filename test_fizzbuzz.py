import pytest
from fizzbuzz import FizzBuzz
from unittest.mock import MagicMock

class TestFizzBuzz:

    @pytest.fixture(scope="function",autouse=True)
    def setup(self):
        print("\nsetup function")
        yield
        print("\nteardown function")

    # object declaration
    @pytest.fixture()
    def fizzbuzz(self):
        fizzbuzz=FizzBuzz()
        return fizzbuzz


     # mocking
    def test_returns_output(self, fizzbuzz, monkeypatch):
        mock_file = MagicMock()
        # expected return value
        mock_file.readline = MagicMock(return_value=30)
        mock_open = MagicMock(return_value=mock_file)
        monkeypatch.setattr("builtins.open", mock_open)
        result = fizzbuzz.readFromFile("input")
        mock_open.assert_called_once_with("input", "r")
        # checking the assert conditions after mocking
        assert True==fizzbuzz.is_divisible_by(result,15)
        assert "fizzbuzz" == fizzbuzz.says(result)

    # checking divisiblity of numbers
    def test_that_number_is_divisible_by_three(self,fizzbuzz):
        assert fizzbuzz.is_divisible_by(3, 3) == True

    def test_that_number_is_NOT_divisible_by_three(self,fizzbuzz):
        assert fizzbuzz.is_divisible_by(1, 3) == False

    def test_that_number_is_divisible_by_five(self,fizzbuzz):
        assert fizzbuzz.is_divisible_by(5, 5) == True

    def test_that_number_is_NOT_divisible_by_five(self,fizzbuzz):
        assert fizzbuzz.is_divisible_by(1, 5) == False

    def test_that_number_is_divisible_by_three_and_five(self,fizzbuzz):
        assert fizzbuzz.is_divisible_by(15, 15) == True

    def test_that_number_is_NOT_divisible_by_three_and_five(self,fizzbuzz):
        assert fizzbuzz.is_divisible_by(1, 15) == False

    # checking testcases
    def test_says_fizz(self,fizzbuzz):
        assert fizzbuzz.says(3) == "fizz"

    def test_says_buzz(self,fizzbuzz):
        assert fizzbuzz.says(5) == "buzz"

    def test_says_fizzbuzz(self,fizzbuzz):
        assert fizzbuzz.says(30) == "fizzbuzz"

    def test_says_nothing(self,fizzbuzz):
        assert fizzbuzz.says(1) == 1
